# alpine-php7
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/alpine-php7)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/alpine-php7)



----------------------------------------
### x64
![Docker Image Version](https://img.shields.io/docker/v/forumi0721/alpine-php7/latest)
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-php7/latest)
### aarch64
![Docker Image Version](https://img.shields.io/docker/v/forumi0721/alpine-php7/aarch64)
![Docker Image Size](https://img.shields.io/docker/image-size/forumi0721/alpine-php7/aarch64)



----------------------------------------
#### Description

* Distribution : [Alpine Linux](https://alpinelinux.org/)
* Architecture : x64,aarch64
* Appplication : [PHP](https://secure.php.net/)
    - PHP is a popular general-purpose scripting language that is especially suited to web development.



----------------------------------------
#### Run

```sh
docker run -d \
           -p 9000:9000/tcp \
           forumi0721/alpine-php7:[ARCH_TAG]
```



----------------------------------------
#### Usage

* URL : [localhost:9000](localhost:9000)



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| 9000/tcp           | FastCGI server listening port                    | 

#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

